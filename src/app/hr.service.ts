import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class HrService {
  private age = 0;
  private lower = 0;
  private upper = 0;

  constructor() { }

  public calculate() {
    this.upper = (220 - this.age) * 0.85;
    this.lower = (220 - this.age) * 0.65;
  }

  public setAge(value) {
    this.age = value;
  }

  public getUpper() {
    return this.upper;
  }

  public getLower() {
    return this.lower;
  }
}
